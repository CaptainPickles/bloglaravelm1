@extends("layouts.layout")

@section('content')
<h1> Mon Blog </h1> 

@if($post)
<form method="post" action="{{route("postUpdate", $post->id)}}"  >
    @csrf
    @method('PUT')
<label>title</label>
<input type="text" id="title" name="title" required
    minlength="1" maxlength="8" size="10" value="{{$post->title}}">
<br>

<label>etrait</label>
<input type="text" id="extrait" name="extrait" required
    minlength="1" maxlength="8" size="10" value="{{$post->extrait}}">
<br>
<label>description</label>     

<textarea id="description" name="description"
       rows="5" cols="33" >{{ $post->description}}
</textarea>
<br>
<br>
<button name="submit" type="submit" value="submit" >modifier</button>
</form>

<form method="post" action="{{route("postDelete",$post->id)}}">
    @csrf
    @method('DELETE')
    <button name="submit" type="submit"  >delete</button>
</form>

<h2>Commentaires</h2>
@foreach($post->comments as $comment)
<li>{{ $comment->content}}</li>
<form method="post" action="{{route("commentDelete", $comment->id)}}">
    @csrf
    @method('DELETE')
    <button name="submit" type="submit" >Delete</button>
</form>
@endforeach


<form method="post" action="{{route("commentAdd", $post->id)}}">
    @csrf
    <label>Votre commentaire</label>
    <input type="text" name="content" required>
    <button type="submit" class="" >Commenter</button>
</form>

<img src="{{asset("storage/$post->picture")}}">
{{-- <ul>

        <li>
            <div>{{ $post->title }}</div>
            <div>{{ $post->description }}</div>
            <div>{{ $post->extrait }}</div>
        </li>
</ul> --}}
<a href="{{route('postDetails')}}">Retourner a la liste des posts</a>
@else 
        <p> Il n'y a aucun article </p
    @endif
    


@endsection